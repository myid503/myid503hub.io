# 灵山精舍

灵山一会，俨然未散。

## 资料整理

1. [索达吉堪布法宝资料汇集](https://github.com/myid503/khenposodargye)
1. [法师辅导资料汇集](https://github.com/myid503/fudao)

## 更新课程

- 《赞僧功德经》讲记 
- 《佛说父母恩难报经》讲记 
- 《苦乐道用》——苦乐转为道用的窍诀（全十讲）


